/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dangnhap;


import VSC.jdbc.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * @author Tuan
 */
@Component
public class UserDAOImp implements UserDAO {

    @Autowired
    @Resource(name = "dataSource")
    DataSource dataSource;

    //DataSource dataSourceFW_config=ConfigDataSource.setDataSourceFW();
    @Override
    public List dangnhap(UserObj user) {
        String sql = "call DANGNHAP_F(?,?)#c,s,s";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForList(sql, new Object[]{user.getTendangnhap(), user.getMatkhau()});
    }
}
