/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dangnhap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * @author Tuan
 */
@Controller
public class UserController {

    @Autowired
    SessionFilter SessionFilter;
    @Autowired
    UserDAO userDAO;

    public static boolean isLocalHost;

    @RequestMapping(value = "/dangnhap")
    public String dangnhap(ModelMap mm, HttpSession session) {
        if (SessionFilter.checkSession(session)) {
            return "redirect:/home";
        }
        return "dangnhap";
    }
    @RequestMapping(value = "/login", produces = "text/html; charset=utf-8", method = RequestMethod.POST)
    public @ResponseBody
    String login(@RequestParam(value = "tendn") String tendn, @RequestParam(value = "matkhau") String matkhau, HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        UserObj user = new UserObj();
        user.setTendangnhap(tendn);
        user.setMatkhau(tienich.tienich.encodePass(matkhau).trim());
        if (user != null) {
            List<Map<String, Object>> list = userDAO.dangnhap(user);
            if (!list.isEmpty()) {
                return "SUCCESS";
            }
        }
        //session.setAttribute("Sess_Err", "loi_dangnhap");
        return "FAIL";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/home";
    }
}
